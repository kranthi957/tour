package com.kranthi.Queue;

public class BasicQueue {

	int[] data;

	int front, end;

	public BasicQueue() {
		this(1000);
	}

	public BasicQueue(int size) {
		this.front = -1;
		this.end = -1;
		data = new int[size];
	}

	public int size() {
		if (end == -1 && front == -1)
			return 0;
		else
			return end - front + 1;
	}

	public void enqueue(int element) throws IllegalAccessException {
		if ((end + 1) % data.length == front)
			throw new IllegalAccessException("The Queue is Full");
		else if (front == -1) {
			data[++front] = element;
			end = front;
		} else
			data[++end] = element;
	}

	public int dequeue() throws IllegalAccessException {
		int temp = 0;
		if (size() == 0)
			throw new IllegalAccessException("Can't Dequeue because queue is empty");
		else if (front <= end && size() != 0)
			temp = data[front++];
		else if (front == end) {
			temp = data[front];
			front = -1;
			end = -1;
		}

		return temp;
	}

	public boolean contains(int element) {
		boolean found = false;

		if (size() == 0)
			return found;
		for (int i = front; i < end; i++) {
			if (data[i] == element) {
				found = true;
				break;
			}
		}
		return found;
	}

	public int access(int position) throws IllegalAccessException {
		int index = 0;
		if (size() == 0 && size() < position)
			throw new IllegalAccessException("No items in the queue greater than this position");
		for (int i = front; i <= end; i++) {
			if (index == position) {
				return data[i];

			}
			index++;
		}
		throw new IllegalAccessException("No element found");

	}
}
