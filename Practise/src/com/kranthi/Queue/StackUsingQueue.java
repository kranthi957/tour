package com.kranthi.Queue;

import java.util.ArrayDeque;
import java.util.Queue;

public class StackUsingQueue {

	Queue<Integer> queue = new ArrayDeque<>();

	void push(Integer data) {
		queue.add(data);
	}

	Integer pop() {
		if (queue.size() == 1)
			return queue.remove();
		else if (queue.size() == 2) {
			queue.add(queue.remove());
			return queue.remove();
		} else
			return removeLastElementInserted(queue.size());
	}

	private Integer removeLastElementInserted(int size) {
		size = size - 1;
		if (size == 0)
			return queue.remove();
		Integer x = queue.remove();
		queue.add(x);
		Integer result = removeLastElementInserted(size);

		return result;
	}

	@Override
	public String toString() {
		return "StackUsingQueue [queue=" + queue + "]";
	}

}
