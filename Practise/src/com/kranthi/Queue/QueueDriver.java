package com.kranthi.Queue;

public class QueueDriver {

	public static void main(String[] args) {

		BasicQueue queue = new BasicQueue(100);
		try {
			queue.enqueue(19);
			queue.enqueue(89);
			System.out.println(queue.dequeue());
			queue.enqueue(187);
			queue.enqueue(128);
			System.out.println(queue.contains(188));
			System.out.println(queue.access(2));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

	}

}
