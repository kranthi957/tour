package com.kranthi.Queue;

public class CircularQueueDriver {

	public static void main(String[] args) {
		CircularQueue q = new CircularQueue(5);
		// Inserting elements in Circular Queue
		q.enQueue(14);
		q.enQueue(22);
		q.enQueue(13);
		q.enQueue(-6);
		System.out.println(q.toString());
		// Deleting elements from Circular Queue
		System.out.println("\nDeleted value = " + q.deQueue());
		System.out.println("\nDeleted value =  " + q.deQueue());
		System.out.println(q.toString());
		q.enQueue(9);
		q.enQueue(20);
		q.enQueue(5);
		System.out.println(q.toString());
		q.enQueue(35);
		System.out.println(q.toString());
		System.out.println("\nDeleted value = " + q.deQueue());
		System.out.println("\nDeleted value =  " + q.deQueue());
		
		System.out.println(q.toString());


	}

}
