package com.kranthi.Queue;

import java.util.Arrays;

public class CircularQueue {

	int size = 0, front = -1, rear = -1;
	int[] array = null;

	public CircularQueue(int length) {
		array = new int[length];
		size = length;
	}

	public boolean enQueue(int data) {
		boolean flag = false;
		if (rear == size - 1 && front == 0 || front - 1 == rear) {
			System.out.println("Queue is full");
			flag = false;
		} else if (rear == size - 1 && front != 0) {
			rear = 0;
			array[rear] = data;
			flag=true;
		} else if (front == -1) {
			front = rear = 0;
			array[front] = data;
			flag = true;
		} else {
			rear++;
			array[rear] = data;
			flag = true;
		}
		return flag;
	}

	public int deQueue() {

		if (front == -1)
			System.out.println("Queue is empty");
		int data = array[front];
		array[front] = -1;
		if (front == rear)
			front = rear = -1;
		else if (front == size - 1) {
			front = 0;
		} else
			front++;
		return data;
	}

	@Override
	public String toString() {
		return "CircularQueue [array=" + Arrays.toString(array) + "]";
	}

}
