package com.kranthi.Queue;

public class StackUsingQueueDriver {

	public static void main(String[] args) {

		StackUsingQueue stack = new StackUsingQueue();

		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(7);
		stack.push(8);
		

		System.out.println(stack);
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());

	}

}
