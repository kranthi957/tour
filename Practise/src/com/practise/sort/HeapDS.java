package com.practise.sort;

import java.util.Arrays;

public class HeapDS {

	int[] arr;
	int size = 0;

	public HeapDS(int data) {
		arr = new int[data];
	}

	public void insert(int data) {

		arr[size] = data;
		size++;
		if (size > 0)
			heapifyUP();
		
	}

	private void heapifyUP() {
		int temp = (size - 1) / 2;
		int index = size - 1;
		while (temp >= 0) {
			if (arr[temp] < arr[index]) {
				swap(temp, index);
				index = temp;
				temp = index / 2;
			} else
				break;
		}

	}

	private void swap(int temp, int index) {
		int t = arr[temp];
		arr[temp] = arr[index];
		arr[index] = t;

	}

	public int remove() {
		int data = arr[0];
		arr[0] = arr[size - 1];
		heapifyDown();
		size--;
		return data;
	}

	private void heapifyDown() {
		int i = 0;
		int smaller = 0;

		while (size > 2 * i + 1) {

			smaller = 2 * i + 1;

			if ((size > 2 * i + 2) && arr[2 * i + 1] < arr[2 * i + 2]) {
				smaller = 2 * i + 2;
			}
			if (arr[i] < arr[smaller]) {
				swap(i, smaller);
				i = smaller;
			} else
				break;

		}

	}

	@Override
	public String toString() {
		return "HeapDS [Max Heap=" + Arrays.toString(arr) + "]";
	}

}
