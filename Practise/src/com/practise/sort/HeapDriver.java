package com.practise.sort;

public class HeapDriver {

	public static void main(String[] args) {

		HeapDS heap = new HeapDS(10);

		heap.insert(15);
		heap.insert(20);
		heap.insert(3);
		heap.insert(2);
		heap.insert(15);
		heap.insert(5);
		heap.insert(4);
		heap.insert(45);
		System.out.println(heap.remove());
		System.out.println(heap.remove());
		System.out.println(heap.remove());
		System.out.println(heap.remove());
		System.out.println(heap.remove());
		System.out.println(heap.remove());
		System.out.println(heap.remove());
		System.out.println(heap.remove());
		System.out.println(heap.size);
		heap.insert(15);
		heap.insert(20);
		heap.insert(3);
		heap.insert(200);
		heap.insert(15);
		heap.insert(5);
		heap.insert(4);
		heap.insert(4);
		System.out.println(heap.toString());

	}

}
