import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class JobScheduling {

	public static void main(String[] args) {

		double rate = 0.1;
		Integer[] volumes = { 3, 1, 41, 52, 15, 4, 1, 63, 12  };
		List<Integer> list = Arrays.asList(volumes);
		Collections.sort(list);
		double totalVolume = 0;
		double N = volumes.length-1;
		for (int i = 0; i < volumes.length; i++) {
			totalVolume += Math.pow((1 - rate), N - i) * volumes[i];
		}

		System.out.println(totalVolume);
	}

}
