package org.kranthi.Stack;

import java.util.Stack;

public class ReverseStack {

	public static void main(String[] args) {

		Stack<Integer> stack = new Stack<>();
		stack.push(3);
		stack.push(2);
		stack.push(9);
		stack.push(4);

		reverseStack(stack);

	}

	private static void reverseStack(Stack<Integer> stack) {
		int temp = 0;
		if(!stack.isEmpty()) {
			temp=stack.pop();
			reverseStack(stack);
		}
		System.out.println(temp);
		
	}

}
