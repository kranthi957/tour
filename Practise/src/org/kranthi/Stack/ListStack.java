package org.kranthi.Stack;

import java.util.ArrayList;
import java.util.List;

public class ListStack {

	private List<Integer> data;
	int count = 0;

	public ListStack() {
		data = new ArrayList<>();
	}

	public void push(int element) {
		data.add(element);
		count++;
	}

	public int pop() throws IllegalAccessException {
		if (count == 0) {
			throw new IllegalAccessException("No More items on stack");
		}
		return data.remove(--count);
	}

	public boolean contains(int element) {
		for (int i = 0; i < count; i++) {
			if (data.get(i) == element) {
				return true;
			}
		}
		return false;
	}

	public int access(int element) throws IllegalAccessException {
		while (count > 0) {
			int temp = pop();
			if (temp == element)
				return temp;
		}
		throw new IllegalAccessException("Could Not Found Item On Stack");
	}

	public int size() {
		return count;
	}
}
