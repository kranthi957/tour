package org.kranthi.Stack;

import java.util.Scanner;
import java.util.Stack;

public class StackSpanProblem {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		int n = in.nextInt();

		while (n > 0) {
			int days = in.nextInt();
			int[] chocolates = new int[days];
			for (int i = 0; i < days; i++) {
				chocolates[i] = in.nextInt();
			}
			int[] result = solveStockSpan(chocolates);
			print(result);
			n--;
		}

	}

	private static void print(int[] result) {

		for (int i = 0; i < result.length; i++) {
			System.out.print(result[i]+" ");
		}
		System.out.println("\n");
	}

	private static int[] solveStockSpan(int[] chocolates) {
		int[] arr = new int[chocolates.length];
		arr[0] = 1;
		Stack<Integer> stack = new Stack<>();
		stack.push(0);
		for (int i = 1; i < chocolates.length; i++) {

			while (!stack.empty() && chocolates[stack.peek()] <= chocolates[i]) {
				stack.pop();

			}

			arr[i] = stack.empty() ? i + 1 : i - stack.peek();
			stack.push(i);

		}
		return arr;

	}

}
