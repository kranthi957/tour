package org.kranthi.Stack;

import java.util.Arrays;
import java.util.Stack;

public class ScareQuotientScore {

	public static void main(String[] args) {

		int[] input = { 6, 2, 5, 4, 5, 1, 6 };
		int[] scores = solveScareQuotientScore(input);
		System.out.println(Arrays.toString(scores));
	}

	private static int[] solveScareQuotientScore(int[] input) {
		int[] result = new int[input.length];
		Stack<Integer> stack = new Stack<>();
		stack.push(0);
		result[0] = 1;

		for (int i = 0; i < input.length; i++) {

			if (!stack.empty() && input[i] >= input[stack.peek()]) {
				stack.pop();
			}

			result[i] = stack.empty() ? i + 1 : i - stack.peek();
			stack.push(i);
		}

		return result;
	}

}
