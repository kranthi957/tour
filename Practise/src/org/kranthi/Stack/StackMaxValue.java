package org.kranthi.Stack;

import java.util.Stack;

public class StackMaxValue {
	static Stack<Integer> stack = new Stack<>();
	static Stack<Integer> maxValueStack = new Stack<>();

	public static void main(String[] args) {

		insert(10);
		insert(100);
		insert(89);
		insert(18);
		insert(679);
		System.out.println(maxValueStack.peek());
		remove();

	}

	private static void remove() {
		if (stack.empty())
			return;
		else {
			stack.pop();
			maxValueStack.pop();
			System.out.println("Current Max value " + maxValueStack.peek());
		}

	}

	public static void insert(int element) {
		if (stack.isEmpty()) {
			stack.push(element);
			maxValueStack.push(element);
		} else {
			stack.push(element);
			maxValueStack.push((element > maxValueStack.peek()) ? element : maxValueStack.peek());
		}
	}

}
