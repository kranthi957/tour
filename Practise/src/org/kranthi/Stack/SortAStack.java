package org.kranthi.Stack;

import java.util.Stack;

public class SortAStack {

	public static void main(String[] args) {

		Stack<Integer> stack = new Stack<>();

		stack.push(45);
		stack.push(37);
		stack.push(15);
		stack.push(60);
		stack.push(10);

		sortGivenStack(stack);
		System.out.println(stack);
	}

	private static void sortGivenStack(Stack<Integer> stack) {
		if (!stack.empty()) {
			int temp = stack.pop();
			sortGivenStack(stack);
			sortedInsert(stack, temp);
		}

	}

	private static void sortedInsert(Stack<Integer> stack, Integer pop) {
		System.out.println(stack);
		if (stack.empty() || stack.peek() > pop)
			stack.push(pop);
		else {
			int temp = stack.pop();
			sortedInsert(stack, pop);
			stack.push(temp);
		}

	}

}
