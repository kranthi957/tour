package org.kranthi.Stack;

public class StackDriver {

	public static void main(String[] args) {

		ListStack stack = new ListStack();

		try {
			stack.push(10);
			stack.push(100);
			stack.push(89);
			System.out.println(stack.pop());
			System.out.println(stack.contains(89));
			stack.access(10);
			System.out.println(stack.size());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

	}

}
