package org.kranthi.Stack;

import java.util.Stack;

public class LargestArea {

	public static void main(String[] args) {

		int[] input = { 6, 2, 5, 4, 5, 1, 6 };
		int maxArea = solveLargeArea(input);
		System.out.println(maxArea);

	}

	private static int solveLargeArea(int[] input) {
		Stack<Integer> stack = new Stack<>();
		int area = 0, max_area = 0;
		int i = 0;
		while (i < input.length) {

			if (stack.empty() || input[i] >= input[stack.peek()])
				stack.push(i++);

			else {

				area = input[stack.pop()] * (stack.empty() ? i : i - stack.peek() - 1);
				max_area = max_area > area ? max_area : area;

			}

		}

		while (!stack.empty()) {
			area = input[stack.pop()] * (stack.empty() ? i : i - stack.peek() - 1);
			max_area = max_area > area ? max_area : area;
		}
		return max_area;
	}

}