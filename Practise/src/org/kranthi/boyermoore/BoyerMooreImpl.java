package org.kranthi.boyermoore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class BoyerMooreImpl {

	public static void main(String[] args) {

		String text = "earc This is boyer moore search algorithm";
		String pattern = "earc";

		List<Integer> indexes = checkPattern(text, pattern);
		Consumer<Integer> consumer = System.out::println;
		indexes.stream().forEach(consumer);
	}

	private static List<Integer> checkPattern(String text, String pattern) {
		int n = pattern.length();
		int m = text.length();
		List<Integer> matches = new ArrayList<>();
		Map<Character, Integer> map = loadBadCharacterTable(pattern);
		int alignedAt = 0, indexInText = 0, indexInPattern = 0;

		while (m > alignedAt + n - 1) {

			for (indexInPattern = n - 1; indexInPattern >= 0; indexInPattern--) {
				indexInText = alignedAt + indexInPattern;
				if (indexInText >= m)
					break;
				char x = text.charAt(indexInText);
				char y = pattern.charAt(indexInPattern);
				if (x != y) {
					Integer val = map.get(y);
					if (val == null) {
						alignedAt = indexInText + 1;
					} else {
						int shift = n - val;
						alignedAt += shift > 0 ? shift : 1;
					}
					break;
				} else if (indexInPattern == 0) {
					matches.add(alignedAt);
					alignedAt++;
				}
			}
		}
		return matches;
	}

	private static Map<Character, Integer> loadBadCharacterTable(String pattern) {
		char[] characters = pattern.toCharArray();
		Map<Character, Integer> map = new HashMap<>();
		int i = 0;
		for (char ch : characters) {
			map.put(ch, i);
			i++;
		}
		return map;
	}

}
