package org.kranthi;

import java.util.LinkedList;

public class InteresectionOfTwoSortedLinkedList {

	public static void main(String[] args) {

		LinkedList<Integer> list = new LinkedList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);

		LinkedList<Integer> list2 = new LinkedList<>();

		list2.add(2);
		list2.add(4);
		list2.add(6);
		list2.add(8);
		list2.add(10);

		printIntersection(list, list2, 0, 0);
	}

	private static void printIntersection(LinkedList<Integer> list, LinkedList<Integer> list2, int i, int j) {

		while (i < list.size() && j < list2.size()) {
			int n = list.get(i);
			int m = list2.get(j);
			if (m == n) {
				System.out.println(m);
				printIntersection(list, list2, i + 1, j + 1);
			} else if (m < n)
				printIntersection(list, list2, i, j + 1);
			else if (n < m)
				printIntersection(list, list2, i + 1, j);
		}

	}

}
