package org.kranthi;

import java.util.LinkedList;

public class PairWiseSwap {

	public static void main(String[] args) {

		LinkedList<Integer> list = new LinkedList<>();
		list.add(2);
		list.add(10);
		list.add(4);
		list.add(6);

		pairwiseSwap(list);
		System.out.println(list.toString());
	}

	private static void pairwiseSwap(LinkedList<Integer> list) {

		int i = 0;

		while (list.get(i) != null && list.get(i + 1) != null) {
			swap(list, i);
			i = i + 2;
			if (i >= list.size())
				break;
		}

	}

	private static void swap(LinkedList<Integer> list, int i) {

		int n = list.get(i);
		int m = list.get(i + 1);
		list.set(i, m);
		list.set(i + 1, n);

	}

}
