package org.kranthi.sort;

import java.util.Arrays;

public class MergeSort {

	public static void main(String[] args) {

		int[] arr = { 4, 100, 2, 1, 8, 7, 2, 10, 6, 1,9 };

		mergeSort(arr, 0, arr.length - 1);
		System.out.println(Arrays.toString(arr));

	}

	private static void mergeSort(int[] arr, int front, int end) {
		if (front >= end)
			return;
		int middle = (front + end) / 2;
		mergeSort(arr, front, middle);
		mergeSort(arr, middle + 1, end);
		merge(arr, front, middle, end);

	}

	private static void merge(int[] arr, int front, int middle, int end) {
		int n = middle - front + 1;
		int m = end - middle;
		int[] left = new int[n];
		int[] right = new int[m];
		int i = 0, j = 0;
		int temp = front;
		for (i = 0; i < left.length; i++) {
			left[i] = arr[temp];
			temp++;
		}
		temp = middle + 1;
		for (j = 0; j < right.length; j++) {
			right[j] = arr[temp];
			temp++;
		}

		int k = front;
		j = 0;
		i = 0;

		while (i < n && j < m) {
			if (left[i] < right[j]) { // change here for ascending or descending
				arr[k] = left[i];
				k++;
				i++;
			} else {

				arr[k] = right[j];
				k++;
				j++;
			}
		}

		while (i < n) {
			arr[k] = left[i];
			i++;
			k++;
		}
		while (j < m) {
			arr[k] = right[j];
			j++;
			k++;
		}

	}

}
