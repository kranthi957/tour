package org.kranthi;

public class LinkedListImpl {

	private Node head=null,tail=null;
	
	int count =0;
	
	public void addFirst(Node in) {
		Node temp=null;
		if(count==0) {
			head=in;
			tail=head;
			count++;
		}
		else {
			temp=head;
			head=in;
			head.setNext(temp);
			count++;
		}
	}
	
	public void addLast(Node in) {
		Node temp=null;
		if(count==0) {
			head=in;
			tail=head;
			count++;
		}
		else {
			tail.setNext(in);
			tail=in;
			count++;
		}
	}
	
	public void removeFirst() {
		if(count!= 0) {
			if(count==1) {
				head=null;
				tail=null;
				count--;
			}
			else {
				head =head.getNext();
				count--;
			}
		}
	}
	
	public void removeLast() {
		
		if(count!=0) {
			if(count==1) {
				head=null;
				tail=null;
				count--;
			}
			else {
				Node temp = head ;
				while(temp!=null) {
					if(temp.getNext()!=tail)
					temp=temp.getNext();
					else
					{
						temp.setNext(null);
						tail=temp;
						count--;
					}
				}
			}
		}
	}
	
	public void printlist() {
		
		Node temp =head;
			
		while(temp.getNext()!=null) {
			System.out.println(temp.getData()+" ");
			temp=temp.getNext();
		}
		System.out.println(temp.getData());
		
	}
	

}
