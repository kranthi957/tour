package org.Practise;

import java.util.Stack;

public class InfixToPostFix {

	public static void main(String[] args) {

		System.out.println(convertToPostfix("A*B-(C+D)+E"));
		System.out.println(convertToPostfix("a+b*(c^d-e)^(f+g*h)-i"));

	}

	private static char[] convertToPostfix(String string) {
		char[] infix = string.toCharArray();
		StringBuffer postfix = new StringBuffer();
		Stack<Character> stack = new Stack<>();

		for (int i = 0; i < infix.length; i++) {
			if (!isOperator(infix[i]))
				postfix.append(infix[i]);
			else if (infix[i] == '(')
				stack.push(infix[i]);
			else if (infix[i] == ')') {
				while (!stack.empty() && stack.peek() != '(') {
					postfix.append(stack.pop());
				}
				if (!stack.empty() && stack.peek() != '(')
					return null;
				else if (!stack.empty())
					stack.pop();
			}

			else if (isOperator(infix[i])) {
				if (!stack.empty() && getPrecedence(infix[i]) <= getPrecedence(stack.peek())) {
					postfix.append(stack.pop());
				}
				stack.push(infix[i]);
			}

		}

		while (!stack.isEmpty()) {
			postfix.append(stack.pop());
		}

		return postfix.toString().toCharArray();
	}

	public static boolean isOperator(char ch) {
		return ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == '(' || ch == ')';
	}

	public static int getPrecedence(char ch) {
		switch (ch) {
		case '+':
		case '-':
			return 1;
		case '^':
			return 3;
		case '*':
		case '/':
			return 2;
		default:
			return -1;

		}
	}

}
