package org.Practise;

public class MagicNumber {

	public static void main(String[] args) {
		Integer i = 982;
		System.out.println(checkMagicNumber(i));
	}

	private static boolean checkMagicNumber(Integer i) {
		
		int temp = 0;
		int sum = i;
		while (i > 9) {
			sum = i;
			temp = 0;
			while (sum != 0) {
				temp = temp + (sum % 10);
				sum = sum / 10;
			}

			i = temp;

		}

		if (i == 1)
			return true;
		else
			return false;

	}
}
