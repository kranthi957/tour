import java.util.ArrayDeque;

public class PrintBinaryNumbers {

	public static void main(String[] args) {

		printBinaryNumbers(10);
	}

	private static void printBinaryNumbers(int i) {

		ArrayDeque<String> queue = new ArrayDeque<>();

		queue.add("1");

		while (i-- > 0) {
			System.out.println(queue.peek());
			String s = queue.poll();
			queue.add(s + "0");
			queue.add(s + "1");
		}

	}

}
